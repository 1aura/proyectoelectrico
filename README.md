# ProyectoElectrico

Este repositorio contiene todos los documentos para el proyecto eléctrico de una pantalla interactiva usando botellas recicladas.

## Biblioteca Video de Processing

Para utilizar la biblioteca Video de processing sin errores por la versión de GStreamer, se requiere usar la versión 2.0 beta4. Este se puede instalar del siguiente sitio:

https://github.com/processing/processing-video/releases/tag/r6-v2.0-beta4
