import gohai.glvideo.*;
import processing.io.*;

SPI spi;
int SIZE = 96;
int HEIGHT = 8;
int WIDTH = 12;
int DEBUG = 0;
// Capture and SPI module declaration
GLCapture camModule;
// Array to hold pixel values
byte[] pixelArray= new byte[SIZE*3];

void setup() {
  // Determine the drawing size and speed
  size(12,8,P2D);
   // Declare the camera name explícitly in order to prevent a NullPointer error
  camModule = new GLCapture(this);
  camModule.start();
  // print SPI list if in DEBUG mode
  if(DEBUG == 1){
    printArray(SPI.list());
  }
  spi = new SPI(SPI.list()[0]);
  spi.settings(1000000, SPI.MSBFIRST, SPI.MODE0); //SPI corre a 20 MHz sin problemas :)
  frameRate(30);
  // update initial values, assume 0
  for (int i = 0; i<SIZE*3; i++){
    pixelArray[i] = byte(0x00);
  }

  
}
void draw() {
  // Update the screen using input from the webcam
  background(0);
  if (camModule.available()) {
    camModule.read();
  }
  image(camModule,0,0,12,8);
  // Load array with 1920 pixel elements using loadPixels
  loadPixels();
  // Update RGB datastructure
  for (int i = 0; i < SIZE; i ++) {
    pixelArray[i] = (byte) (green(pixels[i]));
    pixelArray[i + SIZE] = (byte) (red(pixels[i]));
    pixelArray[i + 2*SIZE] = (byte) (blue(pixels[i]));
  }
  byte[] in = spi.transfer(pixelArray);
}
