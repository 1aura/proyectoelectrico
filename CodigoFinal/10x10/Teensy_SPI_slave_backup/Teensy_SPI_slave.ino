#include "TSPISlave.h"
#include <OctoWS2811.h>

#define WIDTH 10
#define HEIGHT 10
#define STRIP 50
#define CHANNELS 2

const int ledsPerStrip = 50; //100
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
const int config = WS2811_GRB | WS2811_800kHz;
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

uint8_t miso = 1;
uint8_t mosi = 0;
uint8_t sck = 32;
uint8_t cs = 31;
uint8_t spimode = 8;

uint8_t R[STRIP*CHANNELS+1];
uint8_t G[STRIP*CHANNELS+1];
uint8_t B[STRIP*CHANNELS+1];

TSPISlave slaveSPI = TSPISlave(SPI1, miso, mosi, sck, cs, spimode);

void setup() {
  leds.begin();
  // on initialize, turn off all LEDs
  for (int j = 1; j <=(STRIP*CHANNELS); j++) {
    leds.setPixel(j, G[j], R[j], B[j]);
  }
  leds.show();
  slaveSPI.onReceive(spiUpdate);
}

void loop() {
  delay(33);
  updateLEDs();
}

// reads the SPI values.
void spiUpdate() {
  uint8_t temp[] = {0};
  uint16_t i = 0;
  // during transmission
  while ( slaveSPI.active() ) {
    if (slaveSPI.available()) {
      slaveSPI.pushr(temp[i++]);
      if(i <= 1*WIDTH*HEIGHT) {
        G[i] = slaveSPI.popr();
      }
      else if(i <= 2*WIDTH*HEIGHT) {
        R[i-WIDTH*HEIGHT] = slaveSPI.popr();
      }
      else if(i <= 3*WIDTH*HEIGHT){
        B[i-2*WIDTH*HEIGHT] = slaveSPI.popr();
      }
    }
  }
}

// Update LED values based on recent SPI input.
void updateLEDs(){
  uint16_t pixel = 0;
  uint16_t RGBpos = 0;
  // Update all valid LEDs. The rest remain off.
  
  for (int y = 0; y < HEIGHT; y++){
    for (int x = 0; x < WIDTH; x++) {
      if( (y == 1) || (y == 3) || (y == 6) || (y == 8) ){
        pixel = y*HEIGHT+x +1;
        RGBpos = y*HEIGHT+x +1;
      }
      else{
        pixel = y*HEIGHT+x +1;
        RGBpos = (y+1)*HEIGHT-x +1;
      }
      leds.setPixel(pixel, G[RGBpos], R[RGBpos], B[RGBpos]);
    }
  }
  /*
  for (int j = 1; j <=(STRIP*CHANNELS); j++) {
    leds.setPixel(j, G[j], R[j], B[j]);
  }*/
  leds.show();
}
