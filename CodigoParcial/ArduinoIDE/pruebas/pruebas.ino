#include "TSPISlave.h"

#define BUFFER 100


int clk = 32;
int CE0 = 0;
int miso = 1;
int mosi = 31;
int clkSignal[BUFFER];
int csSignal[BUFFER];
int mosiSignal[BUFFER];
int misoSignal[BUFFER];

void setup() {
  pinMode(CE0, INPUT);    // sets the digital pin 7 as input
  pinMode(clk, INPUT);    // sets the digital pin 7 as input
  pinMode(mosi, INPUT);    // sets the digital pin 7 as input
  pinMode(miso, INPUT);    // sets the digital pin 7 as input
  Serial.begin(115200);
}

void loop() {
  for (int i = 0; i < BUFFER; i++){
    clkSignal[i] = digitalRead(clk);
    csSignal[i] = digitalRead(CE0);
    mosiSignal[i] = digitalRead(mosi);
    misoSignal[i] = digitalRead(miso);
    delayMicroseconds(20);
  }
  // clock
  Serial.print("\nCLK:\t");
  for (int i = 0; i < BUFFER; i ++){
    Serial.print(clkSignal[i]);
    Serial.print(" ");
  }
  // chip select
  Serial.print("\nCS: \t");
  for (int i = 0; i < BUFFER; i ++){
    Serial.print(csSignal[i]);
    Serial.print(" ");
  }
  // master output slave input
  Serial.print("\nMOSI: \t");
  for (int i = 0; i < BUFFER; i ++){
    Serial.print(mosiSignal[i]);
    Serial.print(" ");
  }
  // master input slave output
  Serial.print("\nMISO: \t");
  for (int i = 0; i < BUFFER; i ++){
    Serial.print(misoSignal[i]);
    Serial.print(" ");
  }
  Serial.println(" ");
}
