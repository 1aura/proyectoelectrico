#include "TSPISlave.h"
#include <OctoWS2811.h>

const int ledsPerStrip = 50; //100
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
const int config = WS2811_GRB | WS2811_800kHz;
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

uint8_t miso = 1;
uint8_t mosi = 0;
uint8_t sck = 32;
uint8_t cs = 31;
uint8_t spimode = 8;

uint8_t red[100*3];
uint8_t green[100*3];
uint8_t  blue[100*3];

TSPISlave mySPI = TSPISlave(SPI1, miso, mosi, sck, cs, spimode);

void setup() {
  leds.begin();
  leds.show();
  Serial.begin(115200);
  mySPI.onReceive(myFunc);
}

void loop() {
  delay(33);
  updateLEDs();
}

// lee el SPI
void myFunc() {
  uint8_t arr[] = {0};
  uint16_t i = 0;
  int j = 0;
  // during transmission
  while ( mySPI.active() ) {
    if (mySPI.available()) {
      mySPI.pushr(arr[i++]);
      if(i < 40) {
        green[i] = mySPI.popr();
      }
      else if(i < 40*2){
        green[i+10] = mySPI.popr();
      }
      else if(i < 40*3) {
        red[i-80] = mySPI.popr();
      }
      else if(i < 40*4) {
        red[i-80 + 10] = mySPI.popr();
      }
      else if(i < 40*5){
        blue[i-80*2] = mySPI.popr();
      }
      else{
        blue[i-80*2 + 10] = mySPI.popr();
      }
      /*
      if(i < 80) {
        red[i] = mySPI.popr();
      }
      else if(i < 80*2) {
        green[i-80] = mySPI.popr();
      }
      else {
        blue[i-80*2] = mySPI.popr();
      }*/
    }
  }
}

// actualiza los LEDs según los valores del SPI
void updateLEDs(){
  Serial.print("\n UPDATING ");
  Serial.print(leds.numPixels()); 
  Serial.print(" LEDs: RGB[10] 0x");
  Serial.print(red[10],HEX);
  Serial.print(" 0x");
  Serial.print(green[10],HEX);
  Serial.print(" 0x");
  Serial.println(blue[10],HEX);
  for (int j = 1; j < 101; j++) {
    leds.setPixel(j, green[j], red[j], blue[j]);
  }
  leds.show();
}
