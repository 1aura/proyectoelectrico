#include "TSPISlave.h"
#include <OctoWS2811.h>

#define WIDTH 50
#define HEIGHT 2
#define STRIP 50
#define CHANNELS 2

const int ledsPerStrip = 50; //100
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
const int config = WS2811_GRB | WS2811_800kHz;
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

uint8_t miso = 1;
uint8_t mosi = 0;
uint8_t sck = 32;
uint8_t cs = 31;
uint8_t spimode = 8;

uint8_t R[STRIP*CHANNELS+1];
uint8_t G[STRIP*CHANNELS+1];
uint8_t B[STRIP*CHANNELS+1];

TSPISlave mySPI = TSPISlave(SPI1, miso, mosi, sck, cs, spimode);

void setup() {
  leds.begin();
  leds.show();
  mySPI.onReceive(myFunc);
}

void loop() {
  delay(33);
  updateLEDs();
}

// lee el SPI
void myFunc() {
  uint8_t temp[] = {0};
  uint16_t i = 0;
  // during transmission
  while ( mySPI.active() ) {
    if (mySPI.available()) {
      mySPI.pushr(temp[i++]);
      if(i <= 1*WIDTH*HEIGHT) {
        G[i] = mySPI.popr();
        /*
        Serial.print("g");
        Serial.println(i);*/
      }
      else if(i <= 2*WIDTH*HEIGHT) {
        R[i-WIDTH*HEIGHT] = mySPI.popr();
        /*
        Serial.print("r");
        Serial.println(i);*/
      }
      else if(i <= 3*WIDTH*HEIGHT){
        B[i-2*STRIP] = mySPI.popr();
        /*
        Serial.print("b");
        Serial.println(i);*/
      }
    }
  }
}

// actualiza los LEDs según los valores del SPI
void updateLEDs(){
  /*
  Serial.print("\n UPDATING ");
  Serial.println(leds.numPixels()); 
  Serial.print(" LEDs: RGB[10] 0x");
  Serial.print(red[10],HEX);
  Serial.print(" 0x");
  Serial.print(green[10],HEX);
  Serial.print(" 0x");
  Serial.println(blue[10],HEX);*/
  for (int j = 1; j <=(STRIP*CHANNELS); j++) {
    leds.setPixel(j, G[j], R[j], B[j]);
  }
  leds.show();
}
