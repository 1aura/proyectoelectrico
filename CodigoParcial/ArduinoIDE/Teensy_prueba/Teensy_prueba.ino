#include "TSPISlave.h"
#include <OctoWS2811.h>

const int ledsPerStrip = 50; //100
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
const int config = WS2811_GRB | WS2811_800kHz;
OctoWS2811 leds(ledsPerStrip, displayMemory, drawingMemory, config);

uint8_t miso = 1;
uint8_t mosi = 0;
uint8_t sck = 32;
uint8_t cs = 31;
uint8_t spimode = 8;

uint8_t R[100+1];
uint8_t G[100+1];
uint8_t B[100+1];

TSPISlave mySPI = TSPISlave(SPI1, miso, mosi, sck, cs, spimode);

void setup() {
  leds.begin();
  leds.show();
  Serial.begin(115200);
  mySPI.onReceive(myFunc);
}

void loop() {
  delay(33);
  updateLEDs();
}

// lee el SPI
void myFunc() {
  uint8_t temp[] = {0};
  uint16_t i = 0;
  // during transmission
  while ( mySPI.active() ) {
    if (mySPI.available()) {
      mySPI.pushr(temp[i++]);
      if(i <= 10) {
        G[i] = mySPI.popr();
        Serial.print("green_");
        Serial.print(i);
        Serial.print(": ");
        Serial.println(G[i-1]);
      }
      else if(i <= 10*2) {
        R[i-10] = mySPI.popr();
        Serial.print("red_");
        Serial.print(i-10);
        Serial.print(": ");
        Serial.println(R[i-10]);
      }
      else if(i <= 10*3){
        B[i-10*2] = mySPI.popr();
        Serial.print("blue_");
        Serial.print(i-20);
        Serial.print(": ");
        Serial.println(B[i-20]);
      }
    }
  }
  Serial.println(i);
}

// actualiza los LEDs según los valores del SPI
void updateLEDs(){
  Serial.print("\n UPDATING ");
  Serial.println(leds.numPixels()); 
  /*
  Serial.print(" LEDs: RGB[10] 0x");
  Serial.print(red[10],HEX);
  Serial.print(" 0x");
  Serial.print(green[10],HEX);
  Serial.print(" 0x");
  Serial.println(blue[10],HEX);*/
  for (int j = 1; j <= 100; j++) {
    leds.setPixel(j, G[j], R[j], B[j]);
  }
  leds.show();
}
