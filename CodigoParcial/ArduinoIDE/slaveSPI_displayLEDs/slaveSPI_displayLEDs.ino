
// Basado en código ejemplo y libreria:
// https://github.com/tonton81/TSPISlave

// Includes
#include <TSPISlave.h>
#include <OctoWS2811.h>

/*
 * Definiciones
 */

// Constantes
#define ALTURA 32
#define ANCHO 60
#define FPS 30
#define LED_HEIGHT 32
#define LED_WIDTH 60
#define LED_LAYOUT 0

/*
 * Configuración de Librarias
 */
 
// Configure OctoWS2811
const int ledsPerStrip =  LED_WIDTH * LED_HEIGHT / 8; // 250 en total, pero solo 240 en uso
const int config = WS2811_GRB | WS2811_800kHz;
DMAMEM int displayMemory[ledsPerStrip*6];
int drawingMemory[ledsPerStrip*6];
OctoWS2811 leds(ledsPerStrip,displayMemory,drawingMemory,config);

// Configure SPI-slave
uint8_t miso = 51;
uint8_t mosi = 52;
uint8_t sck = 53;
uint8_t cs = 43;
uint8_t spimode = 16;
TSPISlave slaveSPI = TSPISlave(SPI2, miso, mosi, sck, cs, spimode);

/*
 * Declaración de Estructuras de Datos
 */

// Almacenamiento temporal de arreglo rgb
byte red[ALTURA*ANCHO];
byte green[ALTURA*ANCHO];
byte blue[ALTURA*ANCHO];

// Variables
int pos = 0;
bool DEBUG = true;

/*
 * \fn    setup
 * \brief Contains all the initial start-up code
 */ 
void setup() {
  // initializes serial output
  if (DEBUG){
    Serial.begin(115200);
  }
  // run readInputSPI when slaveSPI recieves information
  slaveSPI.onReceive(readInputSPI);    
}

/*
 * \fn    loop
 * \brief Contains the main loop.  
 */
void loop() {
  delay(33);
  Serial.println(millis());
  // cada 33 ms actualizamos el display (cerca de 30FPS)
  updateDisplay();
}

/*
 * \fn    readInputSPI
 * \brief Lee el valor ingresado del SPI, y lo concatena en un arreglo que lee un arreglo de ingresos, y finalmente lo imprime
 */
void readInputSPI() {
  // NOTA: FALTA ACTUALIZAR LA LÓGICA DE SPI QUE SEA IGUAL A PROCESSING
  Serial.println("START: ");
  uint16_t arr[2] = { 0x1111, 0x2222 };
  uint8_t i = 0;
  while (slaveSPI.active() ) {
    if (slaveSPI.available()) {
      slaveSPI.pushr(arr[i++]);             // empujar elemento i al arreglo arr
      Serial.print("VALUE: 0x");
      Serial.println(slaveSPI.popr(), HEX); // sacar elemento i del arreglo arr
    }
  }
  Serial.println("END");
}

/*
 * \fn    
 * \brief Lee el valor ingresado del SPI, y lo concatena en un arreglo que lee un arreglo de ingresos, y finalmente lo imprime
 */
void updateDisplay() {
 //actualizar variables
  pos = 0;
  rojo = 0;
  verde = 0;
  azul = 0;
  // actualizar el display
  for (int i=0; i < ALTURA; i++) {
    for (int j=0; j< ANCHO; j++){
      // obtener valores RGB
      rojo = red[i*ALTURA + j];
      verde = green[i*ALTURA + j];
      azul = blue[i*ALTURA + j];
      // actualizar ese pixel
      leds.setPixel(pos, rojo, verde, azul);
      // leds.setPixel(i, red[i*ALTURA + j], green[i*ALTURA + j], blue[i*ALTURA + j]);//hacerlo todo en 1, cuando ya se depuró?
      // incrementar posición
      pos ++;
    }
  }
  leds.show();
}
