// Utilizando código de:
// http://www.gammon.com.au/spi 
// https://roboticsbackend.com/raspberry-pi-master-arduino-uno-slave-spi-communication-with-wiringpi/
// http://robotics.hobbizine.com/raspiduino.html
// https://forum.arduino.cc/index.php?topic=672278.0

#include <SPI.h>
#include <OctoWS2811.h>

// constantes
#define ALTURA 32
#define ANCHO 60
#define FPS 30

// octo config
const int ledsPerStrip = 240; // 250 en total, pero solo 240 en uso
SPISettings settingsA(10000000, MSBFIRST, SPI_MODE0); //10 MHZ
const int config = WS2811_GRB | WS2811_800kHz;
DMAMEM int displayMemory[ledsPerStrip*6];
OctoWS2811 leds(ledsPerStrip,displayMemory,NULL,config);

// banderas spi
volatile bool flagTransmissionStart = false; //bandera de transmision iniciada
//volatile bool flag2 = false; //bandera de fin de pixel

// variables spi
volatile byte c;
byte i = 0;
volatile byte j = 0;
byte pixArray[1920];



// setup
void setup (void)
{
  // imprimir por puerto serial
  Serial.begin (9600);
  // MISO
  pinMode(MISO, OUTPUT);

  // SPI modo esclavo
  SPCR |= _BV(SPE);
  // encender interrupciones
  SPCR |= _BV(SPIE);

  // octo
  leds.begin();
  leds.show();
}  

void loop (void)
{
   // replantear spi de otra forma
   if((SPSR & (1 << SPIF)) != 0)
   {
     spiHandler();
   }
   if(flagTodoTransmitido) { 
     printScreen();
   }

   delay(5); // cada 5ms
}  

// investigar I2C en paralelo (requiero 1.5Gz, solo hay 1 GHz)
// SPI como esclavo
// comunicación por USB, es UART (rápido)

void spiHandler()
{
   // obtener 1 byte
  c = SPDR;
  
  // si no se ha seteado la bandera de palabraIniciada
  if (flagTransmissionStart == false)
  {
    // elegir un valor "prohibido" de "inicio arreglo"
    // esperar a que mande 0xCD para setear bandera de palabraIniciada (ARREGLAR ESTO, PORQUE xCD ES VALIDO DENTRO DE 0-255)
    // posible idea:, esperar hasta que tire X cantidad de señales CD?

    // de RPi a matriz ancho*alto*3, y estructura de datos -> leds
    if (c == 0xFF)
    {
      SPDR = 0xEF;
      flagTransmissionStart = true;
      j = 0;
    }
  }
  else
  {
    // guardar elemento en el arreglo
    pixArray[j] = c;
    j++;
    // revisar si se obtuvieron todos los elementos del arreglo
    if (j == (3))
    {
      // subir bandera finDeArreglo
      flagTodoTransmitido = true;

      // actualizar ese pixel
      //int i = pixArray[3]*240 + pixArray[4]; //numero de fila * 240 + elemento de esa fila
      //leds.setPixel(i,pixArray[0],pixArray[1],pixArray[2]);
      //leds.show();

      // resetear j y flag 1
      
      j == 0;
      
    }
  }
}
