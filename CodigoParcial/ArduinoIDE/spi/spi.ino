// Utilizando código de:
// http://www.gammon.com.au/spi 
// https://roboticsbackend.com/raspberry-pi-master-arduino-uno-slave-spi-communication-with-wiringpi/
// http://robotics.hobbizine.com/raspiduino.html
// https://forum.arduino.cc/index.php?topic=672278.0

#include <SPI.h>

// banderas
volatile bool flag1 = false; //bandera de  transmision iniciada
volatile bool flag2 = false; //bandera de fin de arreglo
// variables
volatile byte c;
byte i = 0;
volatile byte j = 0;
byte rectArray[7680];

// setup
void setup (void)
{
  // imprimir por puerto serial
  Serial.begin (9600);
  // MISO
  pinMode(MISO, OUTPUT);

  // SPI modo esclavo
  SPCR |= _BV(SPE);
  // encender interrupciones
  SPCR |= _BV(SPIE);
}  

// codigo de interrupciones
ISR (SPI_STC_vect)
{
  // obtener 1 byte
  c = SPDR;
  
  // si no se ha seteado la bandera de palabraIniciada
  if (flag1 == false)
  {
    // esperar a que mande 0xCD para setear bandera de palabraIniciada
    if (c == 0xCD)
    {
      SPDR = 0xEF;
      flag1 = true;
    }
  }
  else
  {
    // guardar elemento en el arreglo
    rectArray[j] = c;
    j++;
    // revisar si se obtuvieron todos los elementos del arreglo
    if (j == (7680))
    {
      // subir bandera finDeArreglo
      flag2 = true;
    }
  }
}

// codigo principal
void loop (void)
{
  // revisar bandera de fin de arreglo
  if (flag2 == true)
  {
    // imprimir todo el arreglo (después implementar código de LEDS)
    for (int k = 0; k < (7680); k++)
    {
      Serial.print(rectArray[k]);
    }
    Serial.println();
    Serial.println("================");
    // bajar banderas y resetar j
    flag1 = false;
    flag2 = false;
    j = 0;
  }
}
