/* NOTA: se importa una versión beta4 de processing.video por errores en versión de GTStreamer*/
import gohai.glvideo.*;
import processing.io.*;

/*
 * Declarar estructuras de datos
 */
 
// Parameters
int ANCHO = 60;
int ALTURA = 32;
int FPS = 30;
boolean DEBUG = true;

// Capture and SPI module declaration
GLCapture webcam;
SPI puertoSPI;

// RGB arrays and variables
byte initialByte = (byte)(0xFE);
byte[] red = new byte[1920];
byte[] green = new byte[1920];
byte[] blue = new byte[1920];
byte[] arraySPI = new byte[5761]; //5761 = 1920*3 + 1 (byte inicial)


/*
 *  \fn    setup
 *  \brief Contains all the code necessary to initialize (setup once)
 */
// se corre al inicio, tipo MAIN
void setup(){
  // Determine the drawing size and speed
  size(60,32,P2D);
  frameRate(FPS);

  // Print the valid cameralist (determine camera name for to beta4 version of Capture library)
  if(DEBUG){
    printArray(GLCapture.list());
    printArray(SPI.list());
  }
   // Declare the camera name explícitly in order to prevent a NullPointer error
  webcam = new GLCapture(this);
  webcam.start();
  
  /**/
  // Empezar con codigo SPI
  puertoSPI = new SPI(SPI.list()[0]);
  puertoSPI.settings(10000000, SPI.MSBFIRST, SPI.MODE0); //10MHz, ideal => tiempo de enviar << draw
  /**/
}

/*
 *  \fn    captureEvent
 *  \brief Reads the webcam at 30fps, which prevents framerate errors.
 */
void captureEvent(GLCapture webcam){
  webcam.read();
}

/*
 *  \fn    draw
 *  \brief First the results are drawn to the image capture screen. Then the results are converted to bytes, and written in the RGB datastructure. Finally, the result is sent via SPI.
 */
void draw(){ 
  // Update the screen using input from the webcam
  background(0);
  image(webcam,0,0,ANCHO,ALTURA);
  // Load array with 1920 pixel elements using loadPixels
  loadPixels();
  // Update RGB datastructure
  for (int i = 0; i < ALTURA; i++) {
    for (int j = 0; j < ANCHO; j++) {
      // Obtain red color
      red[i*ALTURA + j] = (byte) (red(get(i, j)));
      if(red[i*ALTURA + j] == initialByte) {
        red[i*ALTURA + j] = (byte)0xFD;
      }
      // Obtain green color
      green[i*ALTURA + j] = (byte) (green(get(i, j)));
      if(green[i*ALTURA + j] == initialByte){
        green[i*ALTURA + j] = (byte) 0xFD;
      }
      // Obtain blue color
      blue[i*ALTURA + j] = (byte) (red(get(i, j)));
      if(blue[i*ALTURA + j] == initialByte){
        blue[i*ALTURA + j] = (byte) 0xFD;
      }
    }
  }
  // DEBUG code: verify array values and pixel size
  if (DEBUG) {
    printArray(red);
    printArray(green);
    printArray(blue);
    println(pixelWidth, pixelHeight);
  }
  // Obtain arraySPI values
  arraySPI[0] = initialByte;
  for (int i = 1; i < (ALTURA*ANCHO + 1); i++) {
    // Update the 3 corresponding elements in arraySPI
    arraySPI[i*3] = red[i];
    arraySPI[i*3 + 1] = green[i];
    arraySPI[i*3 + 2] = blue[i];
  }
  /*
  // SPI transfer
  byte[] inputSPI = outputSPI.transfer(arraySPI);
  println(inputSPI);
  // if inputSPI = arregloRGB, imprimir y revisar.
  /**/
}
