/* NOTA: se importa una versión beta4 de processing.video por errores en versión de GTStreamer*/
import gohai.glvideo.*;
import processing.io.*;

// Declarar webcam como Capture
GLCapture webcam;
// Declarar modulo SPI
SPI puertoSPI;
// parametros
int ANCHO = 60;
int ALTURA = 32;
int FPS = 30;
boolean DEBUG = false;

// se corre al inicio, tipo MAIN
void setup(){
  size(60,32,P2D);
  // Imprimir lista válida de camaras, (determinar explícitamente, por error de versión beta4)
  if(DEBUG){
    printArray(GLCapture.list());
    printArray(SPI.list());
  }
  // Capturar con declaración explícita de nombre de cámara, prevenir errores de NullPointer
  webcam = new GLCapture(this);
  webcam.start();
    
  // determinar velocidad de draw
  frameRate(FPS);
  
  /**/
  // Empezar con codigo SPI
  puertoSPI = new SPI(SPI.list()[0]);
  puertoSPI.settings(10000000, SPI.MSBFIRST, SPI.MODE0); //10MHz, ideal => tiempo de enviar << draw
  /**/
}

// Despliegar resultado en ventana, establecer frame rate
void draw(){ 
  background(0);
  if (webcam.available()) {
    webcam.read();
  }
  image(webcam,0,0,ANCHO,ALTURA);
  // carga resultado de ventana en un arreglo de 1920 elementos llamado pixel
  loadPixels();
  // convertir a arreglo
  byte[] arregloRGB = {};
  byte[] rowRGB = new byte[ANCHO*3];
  // iterar todas las filas (usando altura)
  for (int i = 0; i < ALTURA; i++) {
    // recorrer contenido de la fila
    for (int j = 0; j < ANCHO; j++) {
      //byte bright = byte(brightness(get(i, j)));
      //byte r = byte(red(get(i, j))); // gets values
      //byte g = byte(green(get(i, j)));
      //byte b = byte(blue(get(i, j)));
      //rowRGB[j*4] = bright;  
      rowRGB[j*3] = byte(red(get(i, j))); 
      rowRGB[j*3+1] = byte(green(get(i, j)));
      rowRGB[j*3+2] = byte(blue(get(i, j)));
    }
    // concatenar el contenido
    arregloRGB = concat(arregloRGB,rowRGB);
  }
  // código de depuración
  if (DEBUG) {
    printArray(arregloRGB); // para depurar, imprime valores del arreglo que se va a imprimir
    println(pixelWidth, pixelHeight); // para depuar, verificar tamaño de pixel
  }
  
  /*
  // Codigo de SPI
  byte[] val = puertoSPI.transfer(arregloRGB);
  println(val);
  // if val = arregloRGB, imprimir y revisar.
  /**/
}
