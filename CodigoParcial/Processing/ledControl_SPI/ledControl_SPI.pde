import processing.io.*;
SPI spi;

int SIZE = 80;

// MCP3001 is a Analog-to-Digital converter using SPI
// datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/21293C.pdf
// see setup.png in the sketch folder for wiring details

// also see AnalogDigital_SPI_MCP3001 for how to write the
// same sketch in an object-oriented way
byte[] allBLUE = new byte[SIZE*3];
byte[] allRED = new byte[SIZE*3];
byte[] allGREEN = new byte[SIZE*3];
byte[] allCORAL = new byte[SIZE*3];
byte[] forestSEA = new byte[SIZE*3];
byte[] blueSHADES = new byte[SIZE*3];
byte[] pastel= new byte[SIZE*3];
byte[] variedPurple = new byte[SIZE*3];
byte[] allBROWN = new byte[SIZE*3];
byte[] dimming = new byte[SIZE*3];
int selector;

void setup() {
  printArray(SPI.list());
  spi = new SPI(SPI.list()[0]);
  spi.settings(100000, SPI.MSBFIRST, SPI.MODE0); //SPI corre a 20 MHz sin problemas :)
  frameRate(1);
  selector = 0;
  // update GREEN
  for (int i = 0; i<SIZE; i++){
    allBLUE[i] = byte(0x00);
    allRED[i] = byte(0x00);
    allGREEN[i] = byte(0xFF);
    allCORAL[i] = byte(0x7F);
    allBROWN[i] = byte(0x8C);
    blueSHADES[i] = byte((80*2-i)*3);
    pastel[i] = byte(i + 80*2);
    dimming[i] = byte(i*i/26);
    if (i < 40){
      forestSEA[i] = byte(0x8B);
      variedPurple[i] = byte(0x00);
    }
    else {
      forestSEA[i] = byte(0xB2);
      variedPurple[i] = byte(0x00);
    }
  }
  // update RED
  for (int i = SIZE; i<SIZE*2; i++){
    allBLUE[i] = byte(0x00);
    allRED[i] = byte(0xFF);
    allGREEN[i] = byte(0x00);
    allCORAL[i] = byte(0xFF);
    allBROWN[i] = byte(0xFF);
    blueSHADES[i] = byte(0x00);
    pastel[i] = byte(i + 80);
    dimming[i] = byte((i-80)*(i-80)/40);
    if (i < 80 + 40){
      forestSEA[i] = byte(0x22);
      variedPurple[i] = byte(0x4B);
    }
    else {
      forestSEA[i] = byte(0x20);
      variedPurple[i] = byte((i-80)*3);
    }
  }
  //update BLUE
  for (int i = SIZE*2; i<SIZE*3; i++){
    allBLUE[i] = byte(0xFF);
    allRED[i] = byte(0x00);
    allGREEN[i] = byte(0x00);
    allCORAL[i] = byte(0x50);
    allBROWN[i] = byte(0x00);
    blueSHADES[i] = byte((i-80*2)*3);
    pastel[i] = byte(i);
    dimming[i] = byte(0x00);
    if (i < 80*2 + 40){
      forestSEA[i] = byte(0x22);
      variedPurple[i] = byte(0x82);
    }
    else {
      forestSEA[i] = byte(0xAA);
      variedPurple[i] = byte(0xDB);
    }
  }
  
}

void draw() {
  selector = (int)random(0,10);
  if(selector == 0){
    selector ++;
    byte[] in = spi.transfer(allGREEN);
  }
  else if (selector == 1){
    selector ++;
    byte[] in = spi.transfer(allCORAL);
  }
  else if (selector == 2){
    selector ++;
    byte[] in = spi.transfer(allBLUE);
  }
  else if (selector == 3){
    selector ++;
    byte[] in = spi.transfer(forestSEA);
  }
  else if (selector == 4){
    selector ++;
    byte[] in = spi.transfer(blueSHADES);
  }
  else if (selector == 5){
    selector ++;
    byte[] in = spi.transfer(pastel);
  }
  else if (selector == 6){
    selector ++;
    byte[] in = spi.transfer(variedPurple);
  }
  else if (selector == 7){
    selector ++;
    byte[] in = spi.transfer(allBROWN);
  }
  else if (selector == 8){
    selector ++;
    byte[] in = spi.transfer(dimming);
  }
  else {
    selector = 0;
    byte[] in = spi.transfer(allRED);
  }
}
