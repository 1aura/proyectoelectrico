/* NOTA: se importa una versión beta4 de processing.video por errores en versión de GTStreamer*/
import gohai.glvideo.*;
import processing.io.*;

/*
 * Definitions
 */
 
// Parameters
int WIDTH = 10;
int HEIGHT = 10;
int FPS = 30;
boolean DEBUG = false;

/*
 * Configure camera and SPI
 */
// Capture and SPI module declaration
GLCapture camModule;
SPI masterSPI;

/*
 * Data structures
 */
byte initialByte = (byte)(0xFE);
byte[] R = new byte[WIDTH*HEIGHT];
byte[] G = new byte[WIDTH*HEIGHT];
byte[] B = new byte[WIDTH*HEIGHT];
byte[] arraySPI = new byte[3*WIDTH*HEIGHT + 2]; //5761 = 1920*3 + 2 (byte inicial y final)


/*
 *  \fn    setup
 *  \brief Contains all the code necessary to initialize (setup once)
 */
void setup(){
  // Determine the drawing size and speed
  size(WIDTH,HEIGHT,P2D);
  frameRate(FPS);

  // Print the valid cameralist (determine camera name for to beta4 version of Capture library)
  if(DEBUG){
    printArray(GLCapture.list());
    printArray(SPI.list());
  }
   // Declare the camera name explícitly in order to prevent a NullPointer error
  camModule = new GLCapture(this);
  camModule.start();
  
  // Setup for SPI transmission
  masterSPI = new SPI(SPI.list()[0]);
  masterSPI.settings(10000000, SPI.MSBFIRST, SPI.MODE0); //10MHz, ideal => sendTime << drawTime
}

/*
 *  \fn    captureEvent
 *  \brief Reads the webcam at 30fps, which prevents framerate errors.
 */
void captureEvent(GLCapture camModule){
  camModule.read();
}

/*
 *  \fn    draw
 *  \brief First the results are drawn to the image capture screen. Then the results are converted to bytes, and written in the RGB datastructure. Finally, the result is sent via SPI.
 */
void draw(){ 
  // Update the screen using input from the webcam
  background(0);
  image(camModule,0,0,WIDTH,HEIGHT);
  
  // Load array with 1920 pixel elements using loadPixels
  loadPixels();
  
  // Update RGB datastructure
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      // Obtain red color
      R[i*WIDTH + j] = (byte) (i*i*j*j/500000);
      // Obtain green color
      G[i*WIDTH + j] = (byte) (i*j/10);
      // Obtain blue color
      B[i*WIDTH + j] = (byte) (i*j/10);
    }
  }
  
  // Update GREEN arraySPI values
  for (int i = 1; i <= WIDTH*HEIGHT; i++) {
    arraySPI[i] = G[i];
  }
  // Update RED arraySPI values
  for (int i = 1; i <= WIDTH*HEIGHT; i++) {
    arraySPI[i + WIDTH*HEIGHT] = R[i];
  }
  // Update BLUE arraySPI values
  for (int i = 1; i <= WIDTH*HEIGHT; i++) {
    arraySPI[i + 2*WIDTH*HEIGHT] = B[i];
  }
  
  // SPI transfer
  byte[] slaveReply = masterSPI.transfer(arraySPI);
  // If DEBUG is enabled, print the reply
  if(DEBUG){
    printArray(slaveReply);
  }
}
