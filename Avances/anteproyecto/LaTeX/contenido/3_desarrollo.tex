% --------------------
  \chapter{Consejos para la escritura y la investigación bibliográfica}
% --------------------
\label{C:desarrollo}

La escritura técnica requiere del seguimiento de algunas normas básicas que buscan la precisión y la homogeneidad. Ante todo, la claridad es clave.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Estilo del informe}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine{T}{odo el informe del proyecto} debe escribirse en \emph{pasado impersonal}, siguiendo las instrucciones generales dadas en este documento. 

Debe cuidarse la redacción del informe, no solo respecto a la ortografía, sino también en cuanto a la estructura gramatical y la puntuación, la cual debe ser conforme a las reglas gramaticales del español.

\paragraph{Unidades}

Debe hacerse uso de las unidades del Sistema Internacional (SI) \cite{RTCR443-2010}, recordando que debe emplearse la coma (,) como separador decimal.

El informe puede escribirse utilizando \LaTeX~ y sus \emph{aspectos de forma} (``el formato''), están predefinidos por la clase \texttt{proyectoelectrico.cls} y no deben modificarse.

Información general sobre el uso de \LaTeX~ se encuentra en el capítulo 4 de este documento de ejemplo. Además, en el folleto de \cite{nsces}, en forma más detallada en el libro de \cite{latexcomp} y en \emph{CervanTeX - Grupo de Usuarios de \TeX~ Hispanohablantes} (\url{http://www.cervantex.es/}), entre muchas otras referencias.

El capítulo \ref{C:desarrollo} y los subsiguientes (si fueran necesarios), mostrarán el trabajo realizado en el proyecto, por lo que su cantidad, títulos y divisiones, se dejan a discreción del estudiante, con la aprobación del profesor guía y demás miembros de tribunal evaluador.

Estos capítulos muestran el ``producto'' del trabajo realizado en el proyecto, por lo cual constituyen la parte medular del informe. Debe explicarse en forma clara: qué se hizo, cómo se hizo y qué se obtuvo.

Cuando se agregan o remueven del texto elementos que aparecen en los índices (general, de figuras, de cuadros) que están en el preámbulo del informe, así como cuando se agregan o remueven citas a las fuentes bibliográficas, que aparecen en la Bibliografía al final del documento, es necesario ejecutar dos o tres veces la compilación del documento, para que estas listas se confeccionen nuevamente y se muestren correctamente.  También, en el caso de agregar o quitar ecuaciones, es necesario recompilar dos o tres veces el documento, para que se renumeren las ecuaciones y  las referencias a estas.

\section{Manejo de las fuentes bibliográficas}
Cuando se realiza un trabajo de desarrollo o investigación, siempre se parte del trabajo realizado por otras personas. Es por lo tanto indispensable, hacer referencia a las fuentes bibliográficas (referencias) utilizadas.

En \LaTeX se utiliza BibTeX para el manejo de la bibliografía.  La información de las fuentes consultadas (libros, artículos de revista o ponencias en congresos, tesis, etc.), se almacenan en un archivo \texttt{.bib} (base de datos de las fuentes bibiográficas), sin preocuparse del formato en que estas serán mostradas en el informe.  Para la creación y manejo de este archivo, se puede utilizar el programa JabRef\footnote{http://jabref.sourceforge.net/} o uno similar.

La forma en que las fuentes son listadas en el apartado Bibliografía, y como son mostradas en el texto cuando se citan, depende del \emph{estilo} seleccionado para esto.

Para el informe del proyecto eléctrico, se debe utilizar el formato APA\footnote{American Psychological Association, http://www.apa.org/}.  En inglés, este se establece utilizando el estilo \texttt{apalike}.  

\begin{equation}
 e^{jx} = \cos{x} + j \sin{x}
\end{equation}

\nomenclature{$j$}{Número imaginario.}

Junto con la clase \texttt{eieproyecto} se suministra el archivo de estilo de bibliografía \texttt{apalike\_es.bst}, en el cual se han cambiado los términos en ingles (ej. ``and'', ``In'', Edition y otros) por su equivalente en español.  Este archivo debe colocarse en la misma carpeta, en donde están los demás archivos utilizados para la confección del informe.

\begin{equation}
\int_0^\infty e^{-x^2} \mathrm{d}x = \frac{\sqrt{\pi}}{2}
\end{equation}

\begin{equation}
\mathbb{A-Z} \quad 
\mathcal{A-Z}
\end{equation}

Por lo tanto, la lista de las fuentes bibliográficas utilizadas se confecciona automáticamente, a partir de las citas hechas en el texto.  Solo las fuentes citadas aparecerán en la bibliografía.

\begin{equation}
u(x) = 
  \begin{cases} 
   \exp{x} & \text{si } x \geq 0 \\
   1       & \text{si } x < 0
  \end{cases}
\end{equation}

Como se indicó anteriormente, se emplean \texttt{cite} y \texttt{citep} para hacer las citas.  Cual de estos dos comandos conviene utilizar, dependerá del contexto en que se haga la cita.  Según la redacción del párrafo, puede convenir que la fuente se indique en el formato ``Autor (año)'', pero en otros casos pudiera ser preferible que esta aparezca en el formato ``(Autor, año)''.

\begin{equation}
r(t) = \Re \left\lbrace \frac{\lambda}{4\pi} \left[ \frac{\sqrt{G_0} u(t) e^{-j2\pi r_0/\lambda}}{r_0} + \frac{\Gamma \sqrt{G_1} u(t-\tau) e^{-j2\pi r_1/\lambda}}{r_1} \right] e^{j2\pi f_c t}  \right\rbrace
\end{equation}

\section{Algunas reglas básicas del español}

\paragraph{\textit{Deber} versus \textit{deber de}} Aunque se parecen, tienen significados distintos: \textit{deber + [infinitivo]} denota obligación, mientras que \textit{deber de + [infinitivo]} expresa posibilidad, suposición, conjetura o creencia \cite{Montolio2002}.

\begin{itemize}
\item Usted \textbf{debe} estudiar más para comprender el tema.
\item Usted \textbf{debe de} ser la hija de esa señora, porque se parecen.
\end{itemize}