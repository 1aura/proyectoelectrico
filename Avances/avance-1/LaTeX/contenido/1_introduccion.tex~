% ----------------------
  \chapter{Introducción}
% ----------------------
\label{C:introduccion}

\lettrine{E}{ste documento} es un ejemplo de uso de la clase de \LaTeX\ llamada \texttt{proyectoelectrico.cls} para el curso \PE. Puede ser utilizado como base para la preparación del informe. El archivo \texttt{proyectoelectrico.cls} debe incluirse en la carpeta donde se encuentran los demás archivos utilizados para la confección del informe (ver sección \ref{S:estructura_archivos}). El archivo fuente que debe compilarse se llama \texttt{proyecto.tex}.

Al leer la versión en formato \texttt{.pdf} de este documento, se recomienda revisar el archivo fuente para contrastar la información incluida en las líneas de comentario, así como los comandos \LaTeX\ utilizados para su elaboración.

\section{¿Qué es el Proyecto Eléctrico?}

La definición de la Escuela de Ingeniería Eléctrica de la Universidad de Costa Rica es la siguiente:

\begin{quote}
El ``Proyecto Eléctrico'' es un curso de un ciclo lectivo bajo la modalidad de trabajo individual supervisado, con el propósito de aplicar estrategias de diseño y análisis a un problema de temática abierta de la ingeniería eléctrica. Es un requisito de graduación para el grado de Cachiller en Ingeniería Eléctrica de la Universidad de Costa Rica.
\end{quote}

Las excepciones y consideraciones especiales se encuentran en el Reglamento del curso de Proyecto Eléctrico, en el Anexo \ref{C:reglamento}.

\paragraph{Cierto tema}

\lipsum[1]

\paragraph{Otro tema interesante}

\lipsum[3]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{El anteproyecto}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

En la Semana 2 del semestre los estudiantes deben entregar a la coordinación del curso \emph{el anteproyecto}, el cual representa la formalización del tema de trabajo junto con los profesores responsables. Aquí se incluye la información más relevante del planteamiento del proyecto. 

\subsection{Contenido del anteproyecto}

El documento incluye la siguiente información:

\begin{description}\ajustado
\item[Identificación del estudiante] Nombre completo y carné.
\item[Título del proyecto] Ver información sobre la composición del título en la sección \ref{S:titulo}. Puede ser actualizado más adelante en el semestre con autorización del profesor guía.
\item[Nombre de los profesores guías y lectores] Ver información sobre el formato de los nombres en la sección \ref{S:profesores}. En el documento impreso final y para los requisitos de graduación se requiere la firma de todos. 
\item[Descripción] Explica el proyecto en uno o dos párrafos de forma concisa.
\item[Objetivos] También sirven de descripción concisa del proyecto. Pueden ser actualizados durante el semestre, bajo la estricta aprobación del profesor guía.
\item[Clasificación temática] una lista de palabras claves del proyecto, según área de estudio.
\item[Declaración de último semestre] donde el estudiante verifica que está en el último semestre. Si no lo está, el profesor guía debe justificar al coordinador por qué es posible levantar ese requisito.
\end{description}

\subsection{Formato del título}\label{S:titulo}

\input{otros/titulo.tex}

\subsection{Formato de los nombres de los profesores}\label{S:profesores}

\input{otros/profesores.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preámbulo del trabajo escrito}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

La parte inicial del informe final está compuesta por la \emph{portada}, la \emph{hoja de aprobación}, el \emph{resumen}, el \emph{abstract} (resumen en inglés), los \emph{reconocimientos}, el \emph{índice general}, el \emph{índice de figuras}, el \emph{índice de tablas} y la \emph{nomenclatura}. Cada parte se describe en las siguientes secciones.

\subsection{Portada}
\label{S:portada}

Para la portada del informe, se debe indicar la siguiente información:

\begin{itemize}\ajustado
\item Institución (universidad, facultad, escuela)
\item Sigla y nombre del curso
\item Título del proyecto\footnote{Ver formato sugerido en la sección \ref{S:titulo}}
\item Nombre completo del estudiante
\item Lugar de presentación
\item Fecha de la entrega del trabajo escrito
\end{itemize}


\subsection{Hoja de aprobación}
\label{S:hoja_aprobacion}

Aquí se incluye la información del(a) profesor(a) guía y los profesores lectores\footnote{Ver formato sugerido en la sección \ref{S:profesores}}. En el informe escrito e impreso, esta hoja debe ser firmada por todos los profesores, como requisito para los trámites de graduación.

\begin{itemize}\ajustado
\item Grado y nombre del profesor guía
\item Grado y nombre de los miembros lectores
\end{itemize}

\subsection{Resumen}

Este debe describir, en forma sucinta, los objetivos, el trabajo realizado, los resultados principales y las conclusiones del proyecto. No debe exceder 250 palabras o una sola página.

El resumen del proyecto es del tipo ``informativo'', por lo tanto:

\begin{center}
\textbf{Qué es el resumen}: \textit{un vistazo del proyecto, de alto nivel}. 

\textbf{Qué no es el resumen}: \textit{una descripción de la metodología}.
\end{center}

\begin{quote}
Según la norma internacional ISO 214-1976 (F), un resumen es la presentación abreviada y acuciosa del contenido de un documento, sin agregar críticas ni interpretaciones y sin indicación de quién escribió el resumen. Los resúmenes informativos proporcionan la esencia del documento, presentan el material más significativo del informe.

\textit{Los resúmenes no relatan las actividades que se desarrollaron, sino que sintetizan los hallazgos}. \cite{Diaz2001}
\end{quote}

\subsubsection{Estructura propuesta del resumen} 

\begin{description}
\item[Primer párrafo] Brevísima explicación del problema (deseo o necesidad) que da origen al proyecto. 

\item[Segundo párrafo] La síntesis del proyecto. Esta es la descripción más sintética que puede darse del trabajo\footnote{El ``elevator pitch'' (pequeño discurso de elevador) denota la acción de describir un plan en una breve interacción con una persona (por ejemplo, en un elevador). Este segundo párrafo podría tener esa función, si se presentara la oportunidad.}.

\item[Tercer párrafo] Las herramientas (analíticas, técnicas, etc.) utilizadas (área de estudio y solución propuesta).

\item[Cuarto párrafo] Enumeración de los resultados principales, síntesis de los hallazgos.
\end{description}

\paragraph{Ejemplo}

\begin{quote}
\begin{quote}
\textit{Primer párrafo} \smallskip \\
En la universidad, la explicación de temas complejos se beneficia de la disponibilidad de documentos escritos para mejorar la comprensión del estudiante.

\textit{Segundo párrafo} \smallskip \\
Este trabajo consiste en la creación de una guía detallada para la elaboración de un trabajo escrito académico y el establecimiento de pautas de escritura e investigación.
\smallskip

\textit{Tercer párrafo} \smallskip \\
El editor de texto \LaTeX\ se utiliza para la creación del documento y la modificación del formato. Para los contenidos, se utilizan fuentes bibliográficas diversas, junto con los consejos de profesores y estudiantes del curso.
\smallskip

\textit{Cuarto párrafo} \smallskip \\
El resultado es un documento que cubre las generalidades de cada sección y que es fácilmente editable.
\end{quote}
\end{quote}

\paragraph{Sobre el formato de la hoja de resumen}

La hoja de resumen contiene todos los datos básicos del proyecto eléctrico, incluyendo una descripción del curso propiamente. Esto hace la hoja útil como un anexo para curriculum o en aplicaciones laborales y académicas, con el fin de mostrar parte del trabajo realizado en la universidad y algunos de los conocimientos adquiridos.

\paragraph{Sobre el ``Abstract''} 
El informe escrito incluye una versión del resumen en inglés. Para esto debe traducir también el título. Se recomienda consultar a personas cercanas con conocimiento en el idioma para revisar la traducción.

\subsection{Reconocimientos}

Una página del informe final contiene la dedicatoria y los agradecimientos. Usualmente la dedicatoria es corta y concisa, y dirigida a pocas personas, mientras que los agradecimientos explican con más detalle sobre las personas y/o instituciones que ayudaron en la elaboración del trabajo.

\subsection{Índices}

Después del resumen se incluirá el índice general  del informe y luego de este el índice de figuras y el índice de tablas .

Las instrucciones en \LaTeX\ para generar los índices son: \texttt{tableofcontents}, \texttt{listoffigures} y \texttt{listoftables}.

\subsection{Nomenclatura}

Todos los símbolos y acrónimos utilizados en las ecuaciones y el texto del informe deben listarse en orden alfabético en la nomenclatura.

Los acrónimos, símbolos de variables o constantes, además de incluirse en la nomenclatura, deben describirse en el texto, pero solo la primera vez que se utilizan.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cuerpo del informe}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

El informe estará constituido por \emph{capítulos} y \emph{secciones} numeradas.

El primer capítulo, ``Introducción'', contiene secciones que son requisito del proyecto (descritas a continuación).

Los capítulos siguientes y su contenido dependerán de la naturaleza del proyecto.

\subsection{Capítulo 1: Introducción}

\begin{quote}
\itshape
La introducción es un capítulo corto inicial que pretende \textbf{delimitar} apropiadamente el trabajo.
\end{quote}

Las secciones que debe incluir dependen de la naturaleza del proyecto y son acordadas entre el estudiante y el profesor guía. Sin embargo, se citan a continuación las secciones usuales y recomendables:

\begin{description}
\item[Párrafos iniciales] proveen una descripción más amplia del proyecto, particularmente del contexto dentro del que se desarrolla (laboratorio de investigación o problema social) y la importancia de sus resultados.
\item[Alcances del proyecto] destaca aquello que incluirá pero sobre todo aquello que no hará el proyecto, a modo de delimitación del trabajo.
\item[Justificación] pretende explicar las razones por las cuales el tema elegido tiene relevancia para el estudiante, o por extensión, para la Escuela, otras personas o el país.
\item[Problema a resolver] es una descripción más detallada y una contextualización del problema. Aquí se debe describir el área del conocimiento en el que se trabaja.
\item[Objetivo general] es la intención del trabajo del proyecto (desde el punto de vista del estudiante, no desde el punto de vista pedagógico).
\item[Objetivos específicos] muestran ``los logros que se pretenden alcanzar con la investigación'' o trabajo \cite{Diaz2001}.
\item[Metodología] describe el \emph{cómo} de la ejecución del proyecto, en una lista de actividades. Estas actividades deben satisfacer cada una uno de los objetivos específicos descritos en la sección anterior.
La interrelación entre la lista de actividades de la metodología, los objetivos específicos y el objetivo general se muestra en la figura \ref{F:objetivos}.
\item[Diagrama de Gantt] es una herramienta (opcional) que describe gráficamente la secuencia de actividades y objetivos en el tiempo.
\item[Último párrafo] debe indicar al lector la estructura de los capítulos siguientes, con una breve descripción del contenido de cada uno y la secuencia lógica de estos.

Ejemplo del último párrafo:

\begin{quote}
En el capítulo 2 se presenta la teoría relacionada con autotransformadores y con redes de mediana tensión, en lo que concierne a estabilidad y utilizando modelos probabilísticos. El capítulo 3 describe el diseño de un modelo de redes en tiempo real utilizando una plataforma de desarrollo programable. El capítulo 4 presenta la propuesta del protocolo de comunicación en caso de falla, mientras que en el capítulo 5 se analizan los resultados de las simulaciones virtuales junto con los equipos de comunicación de fibra óptica. Finalmente, en el capítulo 6 se presentan las conclusiones y recomendaciones.
\end{quote}

\end{description}

\begin{figure}
\centering
\begin{forest}
  	for tree={
    	grow=180,
        l=5cm,
	}
[Objetivo general
	[Objetivo específico 1
    	[Actividad 1.a]
        [Actividad 1.z]
    ]
    [Objetivo específico 2
    	[Actividad 2.a]
        [Actividad 2.z]
  	]
    [Objetivo específico 3
    	[Actividad 3.a]
        [Actividad 3.z]
    ]
]
\end{forest}
\caption{Construcción del objetivo general.}
\label{F:objetivos}
\end{figure}

Este capítulo debe entregarse para revisión de los profesores según el calendario indicado por la cátedra en el programa del curso.

\begin{table}[H]
    \centering
    \begin{tabular}{p{0.5\textwidth}p{0.5\textwidth}}
        \toprule
        \textbf{Objetivos específicos} & \textbf{Pasos metodológicos} \\ \midrule
        Caracterizar las tecnologías actuales en uso para el conteo de tráfico vehicular en función de su costo, complejidad y eficiencia. 
        & 
        \begin{enumerate}
            \item Revisión bibliográfica sobre tecnologías de conteo vehicular utilizadas actualmente.
	        \item Revisión de sensores de diversas índoles, pero bajos costo y complejidad, disponibles en el mercado. 
        \end{enumerate}
        \\ \midrule
        Analizar la viabilidad y el costo de utilizar sensores ultrasónicos para el conteo de tráfico vehicular.
        &
        Lo primero, lo segundo y lo tercero.
        \\ \bottomrule
    \end{tabular}
    \caption{Pasos a seguir para la elaboración del proyecto.}
    \label{T:metodología}
\end{table}

\subsection{Capítulo 2: teoría y antecedentes}

Luego de la introducción, se debe agregar \emph{uno o más capítulos} que discutan la teoría y los antecedentes necesarios para la comprensión y ejecución del proyecto.

\subsection{Otros capítulos}

Según el tratamiento del tema (y la naturaleza del proyecto, ver sección \ref{S:titulo}), el estudiante y el profesor guía pueden optar por ampliar el trabajo escrito con otros capítulos.  Algunos capítulos usuales son:

\begin{multicols}{2}
\begin{itemize}\ajustado
\item Diseño
\item Simulación
\item Implementación
\item Resultados
\item Análisis de resultados
\item Pruebas de ejecución
\end{itemize}
\end{multicols}

\section{Contenido posterior}

\subsection{Apéndices}

Los apéndices incluyen materiales adicionales, ya sea creados por el mismo autor o bien de otras fuentes, que son útiles para la mejor comprensión del trabajo. Algunos apéndices usuales son:

\begin{multicols}{3}
\begin{itemize}\ajustado
\item Cálculos extensos
\item Código fuente (todo o parte de los programas)
\item Hojas de datos de equipos o componentes
\item Teoría básica
\item Entrevistas
\item Noticias relevantes al tema de estudio
\end{itemize}
\end{multicols}

\subsection{Bibliografía}

El formato de la bibliografía es \textit{IEEE Transactions}. Este formato utiliza números encerrados en paréntesis cuadrados para hacer la cita a un material, ejemplo: [1]. En la bibliografía propiamente, hay una lista en orden de aparición dentro del texto (no en orden alfabético por autor).

Sobre cómo manejar esta bibliografía en \LaTeX\ y los sistemas integrados de manejo bibliográfico, consultar la sección \ref{S:citas_bibliograficas}. 

\section{Filosofía del Proyecto Eléctrico}

El Proyecto Eléctrico es único en su tipo en la carrera. Es la primera vez (y quizá la última) que los estudiantes trabajan individualmente.

Según encuestas aplicadas, la gran mayoría de alumnos consultados respalda el curso y su aprendizaje. Aun así, pueden existir dudas respecto a los alcances y la intensión didáctica del curso. La Dirección, la coordinación del curso y el Consejo Asesor podrían optar por definir algunos objetivos generales para los estudiantes y profesores que participan.

\subsubsection{Diseño}

Una característica fundamental del proyecto es la oportunidad de ``diseñar'', en su sentido más amplio. Se puede adoptar la siguiente definición:

\begin{quote}
Diseñar es concebir y dar forma a (artefactos, procesos, diagnósticos, mecanismos, etc.) que resuelven problemas \cite{Ulrich2011}.
\end{quote}

Asimismo, se puede aproximar el proceso de diseño como un proceso iterativo, descrito en el diagrama de la figura \ref{F:ciclo_diseno}.

\begin{figure}[h]
\centering
\begin{tikzpicture}

\def \n {6}
\def \radius {3cm}
\def \margin {13} % en grados

\foreach \s in {1,...,\n}
{
  % Punto
  \node[draw, circle] at ({-360/\n * (\s - 1) + 90}:\radius) {$\s$};
  
  % Flecha
  \draw[<-, >=latex] ({360/\n * (\s - 1) + \margin + 90}:\radius) 
    arc ({360/\n * (\s - 1) + \margin + 90}:{360/\n * (\s)-\margin + 90}:\radius);
}

% Leyendas
\draw ({-360/\n*(1-1)+90}:\radius) node[above=4mm]		{\textbf{Determinar una necesidad}};
\draw ({-360/\n*(2-1)+90}:\radius) node[above right=2mm]{\textbf{Definir el problema}};
\draw ({-360/\n*(3-1)+90}:\radius) node[below right=2mm]{\textbf{Evaluar alternativas}};
\draw ({-360/\n*(4-1)+90}:\radius) node[below=4mm]		{\textbf{Seleccionar plan}};
\draw ({-360/\n*(5-1)+90}:\radius) node[below left=2mm]	{\textbf{Implementar solución}};
\draw ({-360/\n*(6-1)+90}:\radius) node[above left=2mm]	{\textbf{Evaluar solución}};

\end{tikzpicture}
\caption{Etapas de un ciclo de diseño}
\label{F:ciclo_diseno}
\end{figure}

Este ciclo puede ser aplicado a muchos tipos de problemas con muchos productos distintos: puede tratarse de un estudio, análisis, desarrollo, etc. (ver sección \ref{S:titulo}) pero todos completan (o deberían completar) un ciclo como el descrito. Incluso, es recomendable que haga varias iteraciones, con el fin de depurar el resultado.

La definición misma de ingeniería es cercana al diseño. Tómese, por ejemplo, la siguiente definición del Diccionario General de la Lengua Española Vox:

\begin{quote}
\textbf{\textit{ingeniería}}: Arte y técnica de aplicar los conocimientos científicos a la invención, diseño, perfeccionamiento y manejo de nuevos procedimientos en la industria y otros campos de aplicación científicos.
\end{quote}

Se puede inferir, por tanto, que el diseño es un factor fundamental para la ingeniería.

\subsubsection{Integración}

Otra oportunidad valiosa en el Proyecto Eléctrico es la de integrar conocimientos de diversos cursos de la carrera, junto con las investigaciones hechas específicamente para el proyecto. 

Existen conexiones naturales entre los cursos de la carrera (por tratarse de la sucesión de conocimientos de una misma especialidad), pero estas conexiones no serán evidentes sino hasta que haya esfuerzos conscientes de incorporarlas, como en el Proyecto Eléctrico.

Por lo expuesto, es razonable pensar que el proyecto cumple la función de unir y dar coherencia a contenidos que en apariencia estaban dispersos.

\subsubsection{Intención didáctica del Proyecto Eléctrico}

\begin{quote}
El proyecto tiene \textit{el diseño como objetivo} y \textit{la integración de conocimientos como consecuencia}. 
\end{quote}

Si bien estos propósitos no son únicos (también puede existir el interés por desarrollar nuevas teorías científicas o explorar temas completamente nuevos), sí son los más adecuados para los alcances del proyecto, en función de las limitaciones temporales y el nivel académico de los estudiantes. Además, por sí mismos representan objetivos deseables dentro de la formación en ingeniería.